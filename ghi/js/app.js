function createCard(name, description, pictureUrl, starts, ends, location) {
    return `
    <div class="col">
        <div class="card shadow">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                <p class="card-text">${description}</p>
            </div>
            <div class="card-footer">${new Date(starts).toLocaleString('en-GB', { timeZone: 'UTC', day: "numeric", month: "long", year: "numeric" })} - ${new Date(ends).toLocaleString('en-GB', { timeZone: 'UTC', day: "numeric", month: "long", year: "numeric" })}</div>
        </div>
    </div>
    `;
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            console.error('Response failed. error code', error);
        } else {
            const data = await response.json();

            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const name = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const starts = details.conference.starts;
                    const ends = details.conference.ends;
                    const location = details.conference.location.name;
                    const html = createCard(name, description, pictureUrl, starts, ends, location);
                    const column = document.querySelector('.row');
                    column.innerHTML += html;

                }
            }
        }
    } catch (error) {
        console.error('Response incorrect. error code', error);
    }

});
