import React, { useEffect, useState } from 'react';

function ConferenceForm(props) {
    const [locations, setLocations] = useState([]);
    const [formData, setFormData] = useState({
        "name": "",
        "starts": "",
        "ends": "",
        "description": "",
        "max_presentations": "",
        "max_attendees": "",
        "location": ""
    })

    const handleFormChange = (event) => {
        const value = event.target.value;
        const inputName = event.target.name;
        setFormData({
          ...formData,
          [inputName]: value
        })
    }


    const fetchData = async () => {
        const locationUrl = 'http://localhost:8000/api/locations/';

        const response = await fetch(locationUrl);

        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations)
        }

    }

    const handleSubmit = async (event) => {
        event.preventDefault();


        const locationUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(formData),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            setFormData({
                "name": "",
                "starts": "",
                "ends": "",
                "description": "",
                "max_presentations": "",
                "max_attendees": "",
                "location": ""
            })
        }
    }


    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="my-5 container">
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new conference</h1>
                <form onSubmit={handleSubmit} id="create-location-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} placeholder="Name" required type="text" value={formData.name} name="name" id="name" className="form-control"/>
                        <label htmlFor="name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} placeholder="Room count" required type="date" value={formData.starts} id="starts" name="starts" className="form-control"/>
                        <label htmlFor="room_count">Starts</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} placeholder="Room count" required type="date" value={formData.ends} id="ends" name="ends" className="form-control"/>
                        <label htmlFor="room_count">Ends</label>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="exampleFormControlTextarea1" className="form-label">Description</label>
                        <textarea onChange={handleFormChange} className="form-control" value={formData.description} id="description" name="description" rows="3"></textarea>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} placeholder="Room count" required type="number" value={formData.max_presentations} id="max_presentations" name="max_presentations" className="form-control"/>
                        <label htmlFor="room_count">Maximum presentation</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} placeholder="Room count" required type="number" value={formData.max_attendees} id="max_attendees" name="max_attendees" className="form-control"/>
                        <label htmlFor="room_count">Maximum attendees</label>
                    </div>
                    <div className="mb-3">
                        <select onChange={handleFormChange} required id="location" value={formData.location} name="location" className="form-select">
                            <option value="">Choose a location</option>
                            {locations.map(location => {
                                return (
                                    <option key={location.id} value={location.id}>{location.name}</option>
                                );
                            })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
        </div>
    </div>
    );
}

export default ConferenceForm;
