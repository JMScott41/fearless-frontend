import logo from './logo.svg';
import './App.css';
import Nav from "./Nav";
import AttendeesList from './AttendeesList';
import LocationForm  from './LocationForm';
import ConferenceForm from './ConferenceForm';
import PresentationForm from './PresentationForm';
import AttendConferenceForm from './AttendConferenceForm';
import {
  BrowserRouter,
  Route,
  Routes,
} from "react-router-dom";
import MainPage from './MainPage';

function App(props) {

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
          <Routes>
            <Route index element={<MainPage />} />
            <Route path="/conferences/new" element={<ConferenceForm />} />
            <Route path="/attendees/new" element={<AttendConferenceForm />} />
            <Route path="/locations/new" element={<LocationForm />} />
            <Route path="/presentations/new" element={<PresentationForm />} />
            <Route path="/attendees" element={<AttendeesList />} />
          </Routes>
      </div>
    </BrowserRouter>
  );
}

//   return (
//     <>
//     <Nav />
//     <div className="container">
//       <AttendConferenceForm />
//       {/* <ConferenceForm />, */}
//       {/* <LocationForm />, */}
//       {/* <AttendeesList attendees={props.attendees} />, */}
//     </div>
//     </>
//   );
// }

export default App;
