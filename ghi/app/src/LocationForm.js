import React, { useEffect, useState } from 'react';

function LocationForm(props) {
  const [states, setStates] = useState([]);
  const [formData, setFormData] = useState({
    name: '',
    room_count: '',
    city: '',
    state: '',
  })
  

  const handleFormChange = (event) => {
    const value = event.target.value;
    const inputName = event.target.name;
    setFormData({
      ...formData,
      [inputName]: value
    })
  }

  const fetchData = async () => {
    const url = 'http://localhost:8000/api/states/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setStates(data.states)

    }
  }

  const handleSubmit = async (event) => {
    event.preventDefault();


    const locationUrl = 'http://localhost:8000/api/locations/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      setFormData({
        name: '',
        room_count: '',
        city: '',
        state: '',
      })
    }
  }

  useEffect(() => {
    fetchData();
  }, []);


  return (
    <div className="container">
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new location</h1>
          <form onSubmit={handleSubmit} id="create-location-form">
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Name" required type="text" value={formData.name} name="name" id="name" className="form-control"/>
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Room count" value={formData.room_count} required type="number" id="room_count" name="room_count" className="form-control"/>
              <label htmlFor="room_count">Room count</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="City" required type="text" value={formData.city} name="city" id="city" className="form-control"/>
              <label htmlFor="city">City</label>
            </div>
            <div className="mb-3">
              <select onChange={handleFormChange} required id="state" value={formData.state} name="state" className="form-select">
                <option value="">Choose a state</option>
                {states.map(state => {
                  return (
                    <option key={state.abbreviation} value={state.abbreviation}>
                      {state.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  );
}

export default LocationForm;
